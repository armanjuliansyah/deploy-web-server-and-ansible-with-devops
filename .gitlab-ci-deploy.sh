#build testing  and deploy
if [ "$1" == "build" ];then

docker build -t ${IMAGE_NAME_LOCAL_NGINX} -f Dockerfile .
docker build -t ${IMAGE_NAME_LOCAL_ANSIBLE} -f Dockerfile2 .
docker image ls

docker run -d -p 24:22 -p 2021:80 --name web-server ${IMAGE_NAME_LOCAL_NGINX}
docker exec web-server nginx -v

docker run -d -p 25:22 --name ansible-server ${IMAGE_NAME_LOCAL_ANSIBLE}
docker exec ansible-server ansible --version

docker container stop web-server ansible-server
docker container rm web-server ansible-server
docker image rm ${IMAGE_NAME_LOCAL_NGINX} ${IMAGE_NAME_LOCAL_ANSIBLE}


elif [ "$1" == "deploy" ];then

docker build -t ${IMAGE_NAME_GLOBAL_NGINX} -f Dockerfile .
docker build -t ${IMAGE_NAME_GLOBAL_ANSIBLE} -f Dockerfile2 .
docker image ls
docker logout

docker login -u ${username_docker_user} -p ${password_docker_user}
docker push ${IMAGE_NAME_GLOBAL_NGINX}
docker push ${IMAGE_NAME_GLOBAL_ANSIBLE}

docker image rm ${IMAGE_NAME_GLOBAL_NGINX} ${IMAGE_NAME_GLOBAL_ANSIBLE}
docker logout

fi
