#Download base image ubuntu 20.04
FROM ubuntu:20.04

# Pembaharuan sistem
RUN apt-get update && apt-get upgrade -y
RUN apt-get install nano -y

# Buat user
RUN useradd -ms /bin/bash arman

# Konfigurasi SSH
RUN apt-get install openssh-server -y
RUN mkdir /var/run/sshd
RUN echo 'root:arman7890' | chpasswd
RUN echo 'arman:arman123' | chpasswd
RUN sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config

# Instalasi perintah nginx
RUN apt-get install -y nginx

# Menjalankan NGINX dan server pada saat container dijalankan
#CMD ["nginx", "-g", "daemon off;"] && ["/usr/sbin/sshd","-D"]
CMD service ssh start && nginx -g 'daemon off;'

# Forward port dari container menuju external atau host
EXPOSE 22/tcp 80/tcp